# enzyme

Enzyme JavaScript testing utility for React 

<p>
<img src="https://gitlab.com/pascal.cantaluppi/enzyme/-/raw/master/public/enzyme.png" alt="enzyme" width="100" height="100" />
</p>

Enzyme is a JavaScript testing utility for React that makes it easier to assert, manipulate, and traverse your React Components’ output.